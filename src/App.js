import React, { useState, useEffect, useRef, useCallback } from "react";

import {
  GoogleMap,
  useLoadScript,
  Marker,
  Polyline,
  OverlayView,
  Circle
} from "@react-google-maps/api";

import csv from "csv";

const App = () => {
  const [selectedFile, setSelectedFile] = useState();
	const [isSelected, setIsSelected] = useState(false);
  const [isUploadCSV, setUploadCSV] = useState(false);
  const [jsonData, setJsonData] = useState([]);
  const [play, setPlay] = useState(false);
  const [pause, setPause] = useState(false);
  const [incSpeed, setIncSpeed] = useState(false);
  const [decSpeed, setDecSpeed] = useState(false);
  const [inter, setInter] = useState();
  const [map, setMap] = useState(null);
  const [speed, setSpeed] = useState(500);
  const [marker, setMarker] = useState({lat: 13.78911167, lng: 100.60404});
  const[convert, setConvert] = useState('Not Converted');

//{lat: 13.78911167, lng: 100.60404}, {lat: 13.79573333, lng: 100.4097817}
  const containerStyle = {
    width: '600px',
    height: '400px'
  };
  const center = {
    lat: 13.78272167, 
    lng: 100.40788
  };

	const changeHandler = (event) => {
		setSelectedFile(event.target.files[0]);
		setIsSelected(true);
	};

  const uploadHandler = () => {
    setUploadCSV(true);
  }

  const csvToJSON = (file) => {
      const reader = new FileReader();
      let new_state = new Array();
      reader.onload = () => {
        csv.parse(reader.result, (err, data) => {
          if(err){
            console.log("Error in parsing csv: ", csv);
            return;
          }
          console.log("length of data from parser: ", data.length);
          for(var i=1; i< data.length; i++){
            //console.log("data from row: ",i, data[i]);
            let obj = new Object({lat: parseFloat(data[i][1]), lng: parseFloat(data[i][2])})
            new_state.push(obj);
          }

          if(i >= data.length){
            //console.log("the final array is:", new_state);
            setJsonData(new_state);
            //console.log("the data in json state:", jsonData);
            return new_state;
          }
          
        });
      }
      reader.onerror = (err) => {
        console.log("Error in parsing csv", err);
        return null;
      };
  
      reader.readAsBinaryString(file);
  }

  const handlePlay = () => {
    setPlay(true);
  }

  const handlePause = () => {
    setPause(true);
  }

  const handleIncreaseSpeed = () => {
    setIncSpeed(true);
  }

  const handleDecreaseSpeed = () => {
    setDecSpeed(true);
  }

  const { isLoaded, loadError } = useLoadScript({
      googleMapsApiKey: "AIzaSyBtvd6aTmLkNwTS_TqzVfjNGBppvftKo5w",
  });

  const onLoad = useCallback(function callback(map) {
      const bounds = new window.google.maps.LatLngBounds();
      map.fitBounds(bounds);
      setMap(map)
  }, []);

  const onUnmount = useCallback(function callback(map) {
    setMap(null)
  }, []);

  
  useEffect(() => {
    if(isSelected && isUploadCSV){
      let promise = Promise.resolve(csvToJSON(selectedFile));
      promise
      .then((data) => {
        console.log("The received data from csv upload is:", data);
        //setJsonData(data);
        //console.log("the jsonData now is: ", jsonData);
        return;
      })
      // .then(()=> {
      //   console.log("data in json state: ", jsonData);
      // })

      setIsSelected(false);
      setSelectedFile('');
      setUploadCSV(false);
      setConvert("Converted! Please use the controls below the map");
    }
  },[isSelected, isUploadCSV]);

  useEffect(()=>{
    console.log("the change in jsonData: ", jsonData);
  },[jsonData])

  useEffect(() => {
    if(play){
      let data = new Array();
      data = jsonData;
      let count =1;

      let interval = setInterval(function(){
        if(count < data.length){
          setMarker(data[count]);
          count++;
        }
      },speed);
      setInter(interval);
      setPlay(false);
    }
    
    if(pause){
      let clear = inter;
      clearInterval(clear);
      setInter();
      setPause(false);
    }

    if(incSpeed){
      let dec = speed;
      dec -= 100;
      setSpeed(dec);
      setIncSpeed(false);
      let clear = inter;
      clearInterval(clear);
      setInter();
      setPlay(true);
    }

    if(decSpeed){
      let inc = speed;
      inc += 100;
      setSpeed(inc);
      setDecSpeed(false);
      let clear = inter;
      clearInterval(clear);
      setInter();
      setPlay(true);
    }
  },[play, pause, incSpeed, decSpeed]);

  return (
    <div className="">
      <input type="file" name="file" id= "myfile" onChange={changeHandler}/>
      <button onClick={uploadHandler}>convert CSV</button>
      <p>STATUS: {convert} </p>
      <p>SPEED: {10 - (speed/100)}</p>
        {
        isLoaded? (
           <GoogleMap 
              mapContainerStyle={containerStyle} 
              zoom={10} 
              center ={center} 
              onload = {onLoad} 
              onUnmount={onUnmount}
            >
             {/* <Polyline
              path={jsonData}
              options={{ strokeColor: "#FF0000 "}}
             /> */}
             <Marker position={marker}/>
           </GoogleMap>
          ): (<div>Can't Render the map</div>)
        } 
      
      <button onClick={handlePlay}>Play</button>
      <button onClick={handlePause}>Pause</button>
      <button onClick={handleIncreaseSpeed}>Increase speed</button>
      <button onClick={handleDecreaseSpeed}>Decrease speed</button>
    </div>
  );
}

export default App;
